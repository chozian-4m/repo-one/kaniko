# Kaniko

kaniko is a tool to build container images from a Dockerfile, inside a container or Kubernetes cluster.

kaniko doesn't depend on a Docker daemon and executes each command within a Dockerfile completely in userspace.
This enables building container images in environments that can't easily or securely run a Docker daemon,
such as a standard Kubernetes cluster.

Kaniko is a very powerful and complex cli tool. To avoid repeating documentation on
usage, it is highly encouraged to review the official documentation on Kaniko
available [here](https://github.com/GoogleContainerTools/kaniko).

Some important notes on the difference between this and the official image:

## Recommended Resources

Kaniko builds docker images in user space inside it's own container. As such, it needs
as much memory/cpu that would be required depend on the container it is trying to build,
and consumes approximately the same amount of resources that building a similar image with
other build tools (`podman`, `docker`, `builda`) would.

Kaniko does not introduce much overhead itself, therefore true minimum resources
and maximum very depending on what is being built.

This application will build an image, push it out to a remote repository and then exit.
As such, it will need enough space to build the image it's told but this can be ephemeral storage.

## Requires Root User to Run

Kaniko builds a docker image within itself. Therefore, for Kaniko to execute any privileged (root)
commands, Kaniko must be root. As the majority of actions taken in most dockerfiles are privileged,
Kaniko must run as root for the majority use-case.

From Kaniko's documentation [here](https://github.com/GoogleContainerTools/kaniko#security):

```md
The minimum permissions kaniko needs inside your container are governed by a few things:
The permissions required to unpack your base image into its container
The permissions required to execute the RUN commands inside the container
If you have a minimal base image (SCRATCH or similar) that doesn't require permissions to unpack,
and your Dockerfile doesn't execute any commands as the root user, you can run kaniko without root permissions.
It should be noted that Docker runs as root by default, so you still require (in a sense) privileges to use kaniko.
```
