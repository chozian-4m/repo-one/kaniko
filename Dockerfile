ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/docker/scratch
ARG BASE_TAG=ironbank

FROM gcr.io/kaniko-project/executor:v1.8.1-debug as upstream

FROM registry1.dso.mil/ironbank/redhat/ubi/ubi8:8.5 as build

# output image is based on scratch so pipeline is not injecting this
COPY config/ironbank.repo /etc/yum.repos.d/ironbank.repo

RUN rm -f /etc/yum.repos.d/ubi.repo && \
    dnf update -y && \
    dnf install -y gcc make diffutils libstdc++ glibc-static && \
    dnf clean all && \
    rm -rf /var/dnf/cache

COPY bash.tar.gz coreutils.tar.gz musl.tar.gz sed.tar.gz /
COPY config/nsswitch.conf /nsswitch.conf

RUN mkdir -p /usr/local/src/musl && \
    tar -zxf /musl.tar.gz -C /usr/local/src/musl --strip-components=1 && \
    cd /usr/local/src/musl && \
    ./configure --prefix=/musl && \
    make && \
    make install

RUN mkdir -p /usr/local/src/bash && \
    tar -zxf /bash.tar.gz -C /usr/local/src/bash --strip-components=1 && \
    cd /usr/local/src/bash && \
    CC=/musl/bin/musl-gcc CFLAGS="-static -Os" ./configure --without-bash-malloc && \
    make && \
    make tests

RUN mkdir -p /usr/local/src/sed && \
    tar -zxf /sed.tar.gz -C /usr/local/src/sed --strip-components=1 && \
    cd /usr/local/src/sed && \
    ./configure LDFLAGS=-static && \
    make

ENV FORCE_UNSAFE_CONFIGURE=1
RUN cd /usr/lib/gcc/x86_64-redhat-linux/8 && \
    cp crtbeginT.o crtbeginT.o.bak && \
    cp crtbeginS.o crtbeginT.o && \
    mkdir -p /usr/local/src/coreutils && \
    tar -zxf /coreutils.tar.gz -C /usr/local/src/coreutils --strip-components=1 && \
    cd /usr/local/src/coreutils && \
    ./configure && \
    make SHARED=0 CFLAGS="-static -static-libgcc -static-libstdc++ -fPIC"

RUN mkdir /ubi && \
    cp /usr/local/src/bash/bash /ubi/bash && \
    cp /usr/local/src/bash/bash /ubi/sh && \
    cp /usr/local/src/sed/sed/sed /ubi/sed && \
    find /usr/local/src/coreutils -type f -perm 755 -exec mv {} ubi/ \; && \
    chmod 644 /nsswitch.conf

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

COPY --from=upstream /kaniko /kaniko
COPY --from=build /ubi /busybox
COPY --from=build /etc/ssl/certs/ca-bundle.crt /kaniko/ssl/certs/ca-certificates.crt
COPY --from=build /nsswitch.conf /etc/nsswitch.conf

# Declare /busybox as a volume so it gets automatically ignored by kaniko
# GitLabCI BashDetectShellScript hardcodes /busybox/sh so using that instead of /ubi
VOLUME /busybox

ENV HOME /root
ENV USER root
ENV PATH /usr/local/bin:/kaniko:/busybox
ENV SSL_CERT_DIR=/kaniko/ssl/certs
ENV DOCKER_CONFIG /kaniko/.docker/
ENV DOCKER_CREDENTIAL_GCR_CONFIG /kaniko/.config/gcloud/docker_credential_gcr_config.json

WORKDIR /workspace

HEALTHCHECK NONE
ENTRYPOINT ["/kaniko/executor"]
